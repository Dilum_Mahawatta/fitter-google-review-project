﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebScrappingAPI.Models;

namespace WebScrappingAPI.Services
{
    public interface IWebScrappingService
    {
        Task <WebScrapping> GetHtmlDetails(string place_id);
    }
}
