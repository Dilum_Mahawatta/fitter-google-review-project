﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebScrappingAPI.Models;

namespace WebScrappingAPI.Services
{
    public class WebScrappingService : IWebScrappingService
    {
        public async Task<WebScrapping> GetHtmlDetails(string place_id)
        {
            string url = $"https://search.google.com/local/reviews?placeid={place_id}";
            Console.WriteLine(url);
            string html = await GetFinalHtml(url, 20);


            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            var comments = doc.DocumentNode.SelectNodes("//div[@class='jxjCjc']");
            var comment = comments[0];

            

            var list = comments.Select(i =>
            {

                var childNodes = i.ChildNodes;
                var review = i.SelectSingleNode($"{i.LastChild.XPath}/div[2]").InnerText;
                var rate = i.SelectSingleNode($"{i.LastChild.XPath}/div[1]/g-review-stars[1]/span[1]/@aria-label[1]").GetAttributeValue("aria-label", "");

                return new HtmlNodeDetails
                {
                    Name = childNodes[0].InnerText,
                    Comment = review,
                    Rate = rate
                };
            });

            WebScrapping details = new WebScrapping();
            details.PlaceID = place_id;
            details.PlaceAddress = await findplaceName(url);
            details.TotalReviews = list.Count();
            details.Url = url;

            foreach (var item in list)
            {

                HtmlNodeDetails htmlDetails = new HtmlNodeDetails();
                htmlDetails.Name = item.Name;
                htmlDetails.Rate = item.Rate;
                htmlDetails.Comment = item.Comment;
                details.ReviewDetailsList.Add(htmlDetails);

            }

            return await Task.FromResult<WebScrapping>(details);


        }

        private async Task<string> GetFinalHtml(string url, int sectionNum)
        {

            ChromeOptions options = new ChromeOptions();
            options.AddArgument("headless");

            ChromeDriverService driverService = ChromeDriverService.CreateDefaultService();
            driverService.HideCommandPromptWindow = true;

            ChromeDriver driver = new ChromeDriver(driverService, options);

            driver.Navigate().GoToUrl(url);

            Console.WriteLine("Page scrolling, please wait a moment");
            Console.WriteLine(DateTime.Now);
            Thread.Sleep(6000);

           
           var scr1 = driver.FindElementByClassName("review-dialog-list");

    
            for (int i = 1; i <= sectionNum; i++)
             {
               driver.ExecuteScript("arguments[0].scrollTop = arguments[0].scrollHeight", scr1);
               Thread.Sleep(1000);
            }

            Console.WriteLine();
            string html = driver.PageSource; 
            driver.Quit();
            Console.WriteLine(DateTime.Now);
            return await Task.FromResult(html);
        }

        private async Task<string> findplaceName(string url)
        {
            {
                ChromeOptions options = new ChromeOptions();
                options.AddArgument("headless");
                ChromeDriverService driverService = ChromeDriverService.CreateDefaultService();
                driverService.HideCommandPromptWindow = true;
                ChromeDriver driver = new ChromeDriver(driverService, options);
                driver.Navigate().GoToUrl(url);

                String title = driver.Title;
                Console.WriteLine($"Title: {title}");
                return await Task.FromResult(title);
            }

        }

    }
}

