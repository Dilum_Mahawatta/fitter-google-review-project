﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebScrappingAPI.Models
{
    public class WebScrapping
    {
        public string PlaceAddress { get; set; }
        public string PlaceID { get; set; }
        public string Url { get; set; }
        public int TotalReviews { get; set; }
        public virtual List<HtmlNodeDetails> ReviewDetailsList { get; set; } = new();
       
       

    }
}
