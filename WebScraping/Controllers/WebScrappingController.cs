using WebScrappingAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using WebScrappingAPI.Services;

namespace WebScrappingAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class WebScrappingController : ControllerBase
    {
        private readonly IWebScrappingService webScrappingService;

        public WebScrappingController(IWebScrappingService webScrappingService)
        {
            this.webScrappingService = webScrappingService;
        }
        [HttpGet]
        public async Task<IActionResult> GetReviews(string place_id)
        {
            Console.WriteLine(place_id);

            var results = await webScrappingService.GetHtmlDetails(place_id);

            return Ok((WebScrapping)results);
        }



    }
}
